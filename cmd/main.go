package main

import (
	"encoding/xml"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/natebrennand/twiliogo"
	"github.com/natebrennand/twiliogo/sms"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
	"log"
	"net/http"
	"os"
)

// TwiML for responce
type TwiML struct {
	XMLName xml.Name `xml:"Response"`
	Say     string   `xml:",omitempty"`
	Play    string   `xml:",omitempty"`
}

func main() {

	r := mux.NewRouter()

	r.HandleFunc("/", homeHandler).Methods("GET")

	r.HandleFunc("/call", checkCall).Methods("POST")

	r.HandleFunc("/sms", checkSMS).Methods("POST")

	log.Printf("Start serving on :%s \n", os.Getenv("PORT"))

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), r))

}

func homeHandler(w http.ResponseWriter, _ *http.Request) {

	if _, err := fmt.Fprintf(w, "I'm alive!"); err != nil {
		log.Fatalln(err)
	}
}

func checkCall(w http.ResponseWriter, _ *http.Request) {

	tWill := TwiML{Play: "./alice.mp3"}

	x, err := xml.MarshalIndent(tWill, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/xml")

	if _, err := w.Write(x); err != nil {
		log.Fatalln(err)
	}
}

func checkSMS(w http.ResponseWriter, r *http.Request) {

	twilio := twiliogo.NewAccount(os.Getenv("SID"), os.Getenv("TOKEN"))
	if err := r.ParseForm(); err != nil {
		log.Fatalln(err)
	}

	SmsSid := r.PostFormValue("SmsSid")

	smsMessage, err := twilio.Sms.Get(SmsSid)
	if err != nil {
		log.Fatalln(err)
	}

	send2me(smsMessage)

	w.WriteHeader(http.StatusOK)

	twiml := TwiML{Say: "200"}

	x, err := xml.Marshal(twiml)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/xml")

	if _, err := w.Write(x); err != nil {
		log.Fatalln(err)
	}
}

func send2me(mess sms.Message) {

	bot, err := tgbotapi.NewBotAPI(os.Getenv("BOT"))
	if err != nil {
		log.Fatalln(err)
	}

	text := fmt.Sprintf("From: %s \nTime: %s \nBody: %s \n", mess.From, mess.DateSent.String(), mess.Body)

	msg := tgbotapi.NewMessage(int64(377672926), text)

	msg.ParseMode = "HTML"

	if _, err = bot.Send(msg); err != nil {
		log.Fatalln(err)
	}
}
