module phone

go 1.12

require (
	github.com/gorilla/mux v1.7.2
	github.com/natebrennand/twiliogo v0.0.0-20140924180243-e46686c8a915
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	gopkg.in/telegram-bot-api.v4 v4.6.4
)
